package org.mssa.exo14;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

	public static List<String> readLinesFrom(String fileName) {
		Path path = Paths.get(fileName);
		try (Stream<String> lines = Files.lines(path)) {
			return lines.collect(Collectors.toList());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}
	
	public static Long median(Map<Integer, Long> histogram) {
	    int middle = histogram.size()/2;
	    if (histogram.size()%2 == 1) {
	        return histogram.get(middle);
	    } else {
	        return (histogram.get(middle-1) + histogram.get(middle)) / 2;
	    }
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String filename = "C:\\Users\\Mssa-\\OneDrive\\Documents\\eclipse\\eclipse-workspace\\Serie6\\files\\Germinal.txt";

		// Q1 : nb de lignes : 20063 - 322 - 70 = 19671
		List<String> germinal = readLinesFrom(filename);
		germinal = germinal.stream().limit(germinal.size() - 322).skip(70).collect(Collectors.toList());
		Long nbL = germinal.stream().count();
		System.out.println("Nb lignes  : " + nbL);

		// Q2 : nb lignes non vides
		Long nbLnv = germinal.stream().filter(s -> !s.isEmpty()).count();
		System.out.println("Nb lignes non vides : " + nbLnv);

		// Q3 : nb de fois "bonjour"
		Long nbBonjour = germinal.stream().filter(s -> s.contains("Bonjour") || s.contains("bonjour")).count();
		System.out.println("Nb de fois le mot bonjour : " + nbBonjour);

		// Q4
		Function<String, Stream<Character>> strc = (str) -> str.chars().mapToObj(lettre -> (char) lettre);
		System.out.println(strc.apply("test").collect(Collectors.toList()));

		// Q5 : fct Q4 pour Germinal
		Stream<Character> strGerminal = germinal.stream().flatMap(strc);
		// System.out.println(strGerminal.collect(Collectors.toList()));

		// Q6 : liste de ts les caract�res de Germinal
		List<Character> listCarGer = strGerminal.sorted(Comparator.comparing(s -> s)).distinct()
				.collect(Collectors.toList());
		System.out.println("Liste characteres de Germinal : " + listCarGer);
		// Bcp de caract�res ne sont pas des lettres comme !,?,",#

		// Q7
		BiFunction<String, String, Stream<String>> splitWordWithPattern = (line, pattern) -> Pattern
				.compile("[" + pattern + "]").splitAsStream(line);
		// System.out.println(splitWordWithPattern.apply(germinal.toString(), "
		// !\"#$%'()*+ ,\\-./0123456789:;<>?@~").collect(Collectors.toList()));

		Long nbMots = splitWordWithPattern.apply(germinal.toString(), " !\"#$%'()*+ ,\\-./0123456789:;<>?@~").count();
		Long nbMotsdif = splitWordWithPattern.apply(germinal.toString(), " !\"#$%'()*+ ,\\-./0123456789:;<>?@~")
				.distinct().count();
		System.out.println("Nb de mots : " + nbMots);
		System.out.println("Nb de mots diff�rents : " + nbMotsdif);

		// Q8
		int tailleMax = splitWordWithPattern.apply(germinal.toString(), " !\"#$%'()*+ ,\\-./0123456789:;<>?@~")
				.map(s -> s.length()).max(Comparator.naturalOrder()).get();
		Long nbMotsMax = splitWordWithPattern.apply(germinal.toString(), " !\"#$%'()*+ ,\\-./0123456789:;<>?@~")
				.filter(s -> s.length() == tailleMax).count();
		String motsMax = splitWordWithPattern.apply(germinal.toString(), " !\"#$%'()*+ ,\\-./0123456789:;<>?@~")
				.filter(s -> s.length() == tailleMax).collect(Collectors.joining(","));
		System.out.println("Taille du mot le plus long : " + tailleMax);
		System.out.println("Nb de mots les plus long : " + nbMotsMax);
		System.out.println("Mots les plus long : " + motsMax);

		// Q9 : histogramme lgr des mots
		Map<Integer, Long> histogram = splitWordWithPattern
				.apply(germinal.toString(), " !\"#$%'()*+ ,\\-./0123456789:;<>?@~").filter(s -> s.length() >= 2)
				.collect(Collectors.groupingBy(s -> s.length(), Collectors.counting()));
		System.out.println("Histogramme : " + histogram);
		
		int lgrFreqMax = Collections.max(histogram.entrySet(), Map.Entry.comparingByValue()).getKey();
		System.out.println("Longueur de mots la plus fr�quente : " + lgrFreqMax);
		
		System.out.println("Nb de mots avec longueur la plus fr�quente : " + histogram.get(lgrFreqMax));
		
		// Q10
		@SuppressWarnings("rawtypes") //permet d'enlever les warnings sur Iterator
		Iterator iterator = histogram.entrySet().iterator();
		while (iterator.hasNext()) {
			@SuppressWarnings("rawtypes") //permet d'enlever les warnings sur Map.Entry
			Map.Entry entry = (Map.Entry) iterator.next();
			Integer key = (Integer) entry.getKey();
			Long value = (Long) entry.getValue();
			
			if (value == median(histogram)) { 
				System.out.println("Longueur de mot m�diane : " + key);
				System.out.println("Nb de mot de longueur de mot m�diane : " + value);
			}

		}



	}

}
