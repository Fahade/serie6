package org.mssa.exo13;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.List;
//import java.util.stream.Stream;
import java.util.Map;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Creation stream strings
		//Stream<String> strings = Stream.of("one","two","three","four","five","six","seven","eight","nine","ten","eleven","twelve");
		List<String> strings = Arrays.asList("one","two","three","four","five","six","seven","eight","nine","ten","eleven","twelve");
		
		//Q1 : lgr de la chaine la + longue		
		Optional<String> opt1 = strings.stream().max(Comparator.comparing(s->s.length()));
		System.out.println("Longueur max : "+opt1.get().length());

		//Q2 : nb de chaines de lgr paires
		Long nb = strings.stream().filter(s->s.length()%2 == 0).count();		
		System.out.println("Nb de chaines de longueur paire : "+nb);
		
		//Q3 : Liste avec chaines lgr impaires en MAJ
		List<String> impaires = strings.stream().filter(s->s.length() != 0).collect(Collectors.toList());
		//System.out.println("Liste chaines de longueur impaire en MAJ  : "+impaires);
		impaires.forEach(s->System.out.println("En MAJ  : "+s.toUpperCase()));
		
		//Q4
		String conca = strings.stream().filter(s->s.length()<=5).sorted(Comparator.comparing(s->s)).collect(Collectors.joining(",", "{", "}"));
		System.out.println("Concat�nation chaines <=5  : "+conca);
		System.out.println("");

		//Q5 : Cle : lgr chaine  Valeur : liste des chaines associ�es
		Map<Integer,List<String>> stringsMap = strings.stream().collect(Collectors.groupingBy(s->s.length())) ;
		stringsMap.forEach((taille,s)->System.out.println("Longueur chaine : "+taille + ", Liste : " + s));
		System.out.println("");

		//Q6 : Cl� : 1ere lettre  Valeur : concatenation des chaines
		Map<String,String> stringsMap2 = strings.stream().collect(Collectors.groupingBy(s->""+s.charAt(0),Collectors.joining(","))) ;
		stringsMap2.forEach((lettre,s)->System.out.println("1ere lettre : "+lettre + ", Liste : " + s));
	}

}
